stages:
- build
- tests
- provision
- deploy

gitleaks:
  stage: tests
  image:
    name: zricethezav/gitleaks:v8.6.0
    entrypoint: [""]
  script:
    - gitleaks detect -v
  allow_failure: true

docker_build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.5.1-debug
    entrypoint: [""]
  script:
  - mkdir -p /kaniko/.docker/
  - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  - "/kaniko/executor --context ${CI_PROJECT_DIR} --dockerfile ${CI_PROJECT_DIR}/Dockerfile --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA} --destination ${CI_REGISTRY_IMAGE}:latest"

trivy_image:
  stage: tests
  image: docker:19.03
  services:
  - name: docker:19.03-dind
    entrypoint:
      - env
      - "-u"
      - DOCKER_HOST
    command:
      - dockerd-entrypoint.sh
  variables:
    TRIVY_SEVERITY: UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL
    TRIVY_EXIT_ON_SEVERITY: MEDIUM,HIGH,CRITICAL
    TRIVY_EXIT_CODE: 0
    TRIVY_VULN_TYPE: os,library
    TRIVY_NO_PROGRESS: 'false'
    TRIVY_OUTPUT: junit-report.xml
    TRIVY_IGNOREFILE: ".trivyignore"
    TRIVY_CACHE_DIR: ".trivycache/"
    TRIVY_FORMAT: template
    TEMPLATE_NAME: junit.tpl
    TRIVY_CLEAR_CACHE: 'false'
    TRIVY_IGNORE_UNFIXED: 'false'
    TRIVY_DEBUG: 'false'
    TRIVY_OPTIONS: ''
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ''
    TRIVY_VERSION: 0.9.2
    TRIVY_REMOTE: ''
    TRIVY_TIMEOUT: ''
    TRIVY_LIGHT: 'false'
    TRIVY_DOWNLOAD_DB_ONLY: 'false'
    TRIVY_TOKEN: ''
    TRIVY_QUIET: 'false'
    TRIVY_SKIP_UPDATE: 'false'
    CUSTOM_REGISTRY: ''
    REGISTRY_USER: ''
    REGISTRY_PASSWORD: ''
    CUSTOM_TAG: ''
  script:
  - wget https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz
  - tar zxvf trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz
  - wget -O $TEMPLATE_NAME https://github.com/aquasecurity/trivy/raw/v${TRIVY_VERSION}/contrib/junit.tpl
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - REGISTRY_IMAGE=$CI_REGISTRY_IMAGE
  - IMAGE="$REGISTRY_IMAGE:$CI_COMMIT_SHA"
  - ./trivy --template "@${TEMPLATE_NAME}" --cache-dir ${TRIVY_CACHE_DIR} --output ${TRIVY_OUTPUT} ${TRIVY_OPTIONS} $IMAGE
  cache:
    paths:
    - "$TRIVY_CACHE_DIR"
  allow_failure: true

mkdocs:
  image:
    name: squidfunk/mkdocs-material:8.1.4
    entrypoint:
    - ''
  stage: build
  variables:
    MKDOCS_OUTPUT_PATH: website_build/
    MKDOCS_ADDITIONAL_PLUGINS: mkdocs-awesome-pages-plugin;mkdocs-git-revision-date-localized-plugin;mkdocs-macros-plugin
  script:
  - 'if [[ ${MKDOCS_ADDITIONAL_PLUGINS} != "" ]]; then

    '
  - oldIFS=$IFS
  - export IFS=";"
  - for plugin in $MKDOCS_ADDITIONAL_PLUGINS; do
  - IFS=$oldIFS
  - pip3 install $plugin
  - export IFS=";"
  - done fi
  - mkdocs build -d "$MKDOCS_OUTPUT_PATH"
  artifacts:
    when: always
    expose_as: Mkdocs build
    paths:
    - "$MKDOCS_OUTPUT_PATH"
    - website_build/

pages:
  stage: deploy
  variables:
    PAGES_BUILD_PATH: website_build/
  script:
  - if [ "$PAGES_BUILD_PATH" != "public" -a "$PAGES_BUILD_PATH" != "public/" ]; then
  - if [ -d "public" ]; then rm -rf public; fi
  - mv "$PAGES_BUILD_PATH" public
  - fi
  - if [ ! -d "public" ]; then echo "Nothing to deploy"; exit 1; fi
  artifacts:
    paths:
    - public
  rules:
  - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"

pylint:
  image: python:3.10-buster
  stage: tests
  variables:
    PROJECT_ROOT: "."
    PYLINT_OPTIONS: ''
    PYLINT_OUTPUT: report_pylint.xml
    PYLINT_EXIT_ZERO: 'true'
  script:
  - cd $PROJECT_ROOT
  - pip install pylint pylint_junit
  - options="--output-format=pylint_junit.JUnitReporter --output=$PYLINT_OUTPUT"
  - $([ ${PYLINT_EXIT_ZERO} == "true" ]) && options="${options} --exit-zero"
  - pylint ${options} $PYLINT_OPTIONS $(find -type f -name "*.py" ! -path "**/.venv/**")
  artifacts:
    when: always
    paths:
    - "${PROJECT_ROOT}/${PYLINT_OUTPUT}"
    reports:
      junit:
      - "${PROJECT_ROOT}/${PYLINT_OUTPUT}"

trivy_dependency:
  stage: tests
  image:
    name: aquasec/trivy:0.12.0
    entrypoint:
    - ''
  variables:
    TRIVY_SEVERITY: LOW,MEDIUM,HIGH,CRITICAL
    TRIVY_EXIT_ON_SEVERITY: MEDIUM,HIGH,CRITICAL
    TRIVY_EXIT_CODE: 0
    TRIVY_VULN_TYPE: library
    TRIVY_NO_PROGRESS: 'false'
    TRIVY_OUTPUT: junit-report.xml
    TRIVY_IGNOREFILE: ".trivyignore"
    TRIVY_CACHE_DIR: ".trivycache/"
    TRIVY_FORMAT: template
    TRIVY_TEMPLATE_DIRECTORY: "/contrib"
    TEMPLATE_NAME: junit.tpl
    TRIVY_CLEAR_CACHE: 'false'
    TRIVY_IGNORE_UNFIXED: 'false'
    TRIVY_DEBUG: 'false'
    TRIVY_OPTIONS: ''
    TRIVY_VERSION: 0.12.0
    TRIVY_REMOTE: ''
    TRIVY_SKIP_UPDATE: 'false'
  script:
  - trivy fs --template "@${TRIVY_TEMPLATE_DIRECTORY}/${TEMPLATE_NAME}" --cache-dir
    ${TRIVY_CACHE_DIR} --output ${TRIVY_OUTPUT} ${TRIVY_OPTIONS} ./
  - if [ ! -z ${TRIVY_EXIT_ON_SEVERITY} ]; then
  - trivy fs --exit-code 1 --template "@${TRIVY_TEMPLATE_DIRECTORY}/${TEMPLATE_NAME}"
    --cache-dir ${TRIVY_CACHE_DIR} --severity ${TRIVY_EXIT_ON_SEVERITY} --output failed.${TRIVY_OUTPUT}
    ${TRIVY_OPTIONS} ./
  - fi
  cache:
    paths:
    - "${TRIVY_CACHE_DIR}"
  artifacts:
    reports:
      junit:
      - "${TRIVY_OUTPUT}"
    expire_in: 30 days
    when: always
  allow_failure: true

sls_scan:
  image: shiftleft/sast-scan:v1.15.1
  stage: tests
  variables:
    SCAN_OPTIONS: ''
    ENABLE_BUILD: 'true'
    SLS_TYPE: ''
    OUTPUT_PATH: sls_scan_report/
  script:
  - mkdir "$OUTPUT_PATH"
  - if [ ${ENABLE_BUILD} == "true" ]; then
  - SCAN_OPTIONS="--build ${SCAN_OPTIONS}"
  - fi
  - if [ ! -z ${SLS_TYPE} ]; then
  - SCAN_OPTIONS="${SCAN_OPTIONS} -t ${SLS_TYPE}"
  - fi
  - scan ${SCAN_OPTIONS} -o "$OUTPUT_PATH" | tee output
  after_script:
  - touch 1_COMPLETE_REPORT.html && cd $OUTPUT_PATH
  - for report in *.html; do
  - cat $report >> ../1_COMPLETE_REPORT.html
  - done
  artifacts:
    when: always
    expose_as: ShiftLeft security scan
    paths:
    - 1_COMPLETE_REPORT.html
    - "$OUTPUT_PATH"
    - output
    - sls_scan_report/
  allow_failure: true

cache:
  key:
    files:
    - "${CI_PROJECT_DIR}/${TF_ROOT}/.terraform.lock.hcl"
  paths:
  - "${CI_PROJECT_DIR}/${TF_ROOT}/.terraform"

gitlab-terraform_plan:
  stage: provision
  image: registry.gitlab.com/gitlab-org/terraform-images/releases/1.0:v0.19.0
  variables:
    TF_ROOT: "${CI_PROJECT_DIR}/terraform"
    TF_ADDRESS: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/main"
    INIT_OPTIONS: ''
  script:
  - cd ${TF_ROOT}
  - terraform fmt -check -diff ${TF_ROOT}
  - gitlab-terraform init ${INIT_OPTIONS} | tee output.log
  - gitlab-terraform validate | tee -a output.log
  - gitlab-terraform plan | tee -a output.log
  - gitlab-terraform plan-json | tee -a output.log
  - mkdir ${CI_PROJECT_DIR}/terraform_def
  - "(cd ${TF_ROOT}; tar cvf - .) | (cd ${CI_PROJECT_DIR}/terraform_def; tar xvf -)"
  artifacts:
    when: always
    expose_as: Terraform plan artifact
    paths:
    - "${TF_ROOT}/output.log"
    - "${TF_ROOT}/plan.cache"
    - "${CI_PROJECT_DIR}/terraform_def"
    reports:
      terraform:
      - "${TF_ROOT}/plan.json"

gitlab-terraform_apply:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/terraform-images/releases/1.0:v0.19.0
  variables:
    TF_ROOT: "${CI_PROJECT_DIR}/terraform"
    TF_ADDRESS: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/main"
  script:
  - cd ${TF_ROOT}
  - gitlab-terraform apply
  artifacts:
    when: always
    expose_as: Terraform apply artifact
    paths:
    - "${TF_ROOT}/output.log"
  when: manual

